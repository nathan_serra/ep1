#include "submarino.hpp"
#include <string>
#include <iostream>

using namespace std;

Submarino::Submarino(){
  set_nome("Submarino");
  endereco = new int[4];
  qtd_de_tiros = new int[2];
  qtd_de_tiros[1] = 2;
  qtd_de_tiros[2] = 2;
}

Submarino::~Submarino(){
  cout << "SUBMARINO FOI DESTRUIDO!!!" << endl;
}

void Submarino::perder_vida(int linha, int coluna){
  set_tiro("ERROU");
  if(endereco[1] == linha && endereco[2] == coluna){
    qtd_de_tiros[1] --;
    cout << endl << "acertou o submarino!!!!" << endl;
    if(qtd_de_tiros[1] == 0 && qtd_de_tiros[2] == 0){

      set_vida(0);
    }
    set_tiro("ACERTOU");
  }

  else if(endereco[3] == linha && endereco[4] == coluna){
    qtd_de_tiros[2] --;
    cout << endl << "acertou o submarino!!!!" << endl;
    if(qtd_de_tiros[1] == 0 && qtd_de_tiros[2] == 0){

      set_vida(0);
    }
    set_tiro("ACERTOU");
  }

}

void Submarino::enderecar(int linha, int coluna, char direcao){
  switch (direcao) {
    case 'c':
    endereco[1] = linha;
    endereco[2] = coluna;
    linha --;
    endereco[3] = linha;
    endereco[4] = coluna;
    break;

    case 'b':
    endereco[1] = linha;
    endereco[2] = coluna;
    linha ++;
    endereco[3] = linha;
    endereco[4] = coluna;
    break;

    case 'e':
    endereco[1] = linha;
    endereco[2] = coluna;
    coluna --;
    endereco[3] = linha;
    endereco[4] = coluna;
    break;

    case 'd':
    endereco[1] = linha;
    endereco[2] = coluna;
    coluna ++;
    endereco[3] = linha;
    endereco[4] = coluna;
    break;

  }
}
