#include "porta_avioes.hpp"
#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

Porta_avioes::Porta_avioes(){
  set_nome("Porta avioes");
  endereco = new int[8];
  qtd_de_tiros = new int[4];
  qtd_de_tiros[1] = 1;
  qtd_de_tiros[2] = 1;
  qtd_de_tiros[3] = 1;
  qtd_de_tiros[4] = 1;
}

Porta_avioes::~Porta_avioes(){
  cout << "PORTA AVIOES FOI DESTRUIDO!!!" << endl;
}

void Porta_avioes::perder_vida(int linha, int coluna){
  set_tiro("ERROU");
  int num_aleatorio = rand() % 3;
  int j = 1;

  for(int i = 0; i < 4; i++){
    if(num_aleatorio == 2 && endereco[(j)] == linha && endereco[i+i] == coluna){
      qtd_de_tiros[i] --;
      cout << endl << "acertou o porta avioes!!!" << endl;
      if(qtd_de_tiros[1] == 0 && qtd_de_tiros[2] == 0 && qtd_de_tiros[3] == 0 && qtd_de_tiros[4] == 0){
        set_vida(0);
      }
      set_tiro("ACERTOU");
      break;
    }
    j += 2;
  }
}

void Porta_avioes::enderecar(int linha, int coluna, char direcao){
  switch (direcao) {
    case 'c':
    endereco[1] = linha;
    endereco[2] = coluna;
    linha --;
    endereco[3] = linha;
    endereco[4] = coluna;
    linha --;
    endereco[5] = linha;
    endereco[6] = coluna;
    linha --;
    endereco[7] = linha;
    endereco[8] = coluna;

    break;

    case 'b':
    endereco[1] = linha;
    endereco[2] = coluna;
    linha ++;
    endereco[3] = linha;
    endereco[4] = coluna;
    linha ++;
    endereco[5] = linha;
    endereco[6] = coluna;
    linha ++;
    endereco[7] = linha;
    endereco[8] = coluna;
    break;

    case 'e':
    endereco[1] = linha;
    endereco[2] = coluna;
    coluna --;
    endereco[3] = linha;
    endereco[4] = coluna;
    coluna --;
    endereco[5] = linha;
    endereco[6] = coluna;
    coluna --;
    endereco[7] = linha;
    endereco[8] = coluna;
    break;

    case 'd':
    endereco[1] = linha;
    endereco[2] = coluna;
    coluna ++;
    endereco[3] = linha;
    endereco[4] = coluna;
    coluna ++;
    endereco[5] = linha;
    endereco[6] = coluna;
    coluna ++;
    endereco[7] = linha;
    endereco[8] = coluna;
    break;

  }
}
