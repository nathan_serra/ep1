#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "jogador.hpp"
#include "embarcacao.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "porta_avioes.hpp"

using namespace std;

template <typename T1>

T1 getInput(){
    while(true){
        T1 valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}

string getString(){
    while(true){
        string valor;
        try{
            getline(cin, valor);
        }
        catch(const std::ios_base::failure& e){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida - " << e.what() << " - Insira novamente: " << endl;
        }
        return valor;
    }
}

void comecar(Jogador &p1, Jogador &p2){
  int confirmacao;
  system("clear");

  do{
    cout << "                  REGRAS DO JOGO:" << endl;
    cout << "1- Quando iniciado o jogo vc so tera acesso ao mapa do inimigo para saber onde atacar;" << endl;
    cout << "2- A cada rodada cada jogador pode atacar somente uma vez" << endl;
    cout << "3- Ganha o jogo quem fizer 10 pontos primeiro, cada embarcacao aertada ganha somente 1 ponto" << endl;
    cout << "4- Embarcacoes: " << endl << "   Canoa: tem so uma casa e precisa so de um tiro para derruba - la;" << endl <<"   Submarino: tem duas casas e precisa acertar duas vezes a mesma casa para ganhar 1 ponto e duas vezes em cada casa para derruba-lo; " << endl << "   Porta avioes: tem quatro casas, mas para acert-lo vc tem somente 33% de chance, mas sera avisado quando o missel atingir a casa em que ele esta." << endl;
    cout << endl << "se entendeu as regras digite '1':  ";
    confirmacao = getInput<int>();
    system("clear");
  }while(confirmacao != 1);

  string nome;
  cout << "Jogador 1 digitar nome: ";
  nome = getString();
  cout << endl;
  p1.set_nome(nome);

  cout << "Jogador 2 digitar nome: ";
  nome = getString();
  cout << endl;
  p2.set_nome(nome);

  cout << "     JOGO INICIADO!!!!!!" << endl << endl;
  cout << "para comecar o jogo aperte '1':  ";
  do{
    confirmacao = getInput<int>();
  }while(confirmacao != 1);
}

int main(){

  Jogador *j1;
  j1 = new Jogador();
  Jogador *j2;
  j2 = new Jogador();
  comecar(*j1, *j2);

  fstream txtFile;
  txtFile.open ("map_1.txt",  fstream::in);
  if(!txtFile.is_open()){
      cout << "problemas ao abrir o arquivo TXT" << endl << endl;
  }

  vector< char > ler;
  char c;
  while(txtFile.get(c)){
    //ler.push_back(c)
  }

  txtFile.close();
  if(txtFile.is_open()){
    cout << "arquivo TXT nao fechou" << endl << endl;
  }

  //nao consegui fazer a leitura do arquivo   .txt

  vector < Embarcacao * > player1;
  vector < Embarcacao * > player2;

  Canoa *c1 = new Canoa();
  Canoa *c2 = new Canoa();
  Canoa *c3 = new Canoa();
  Canoa *c4 = new Canoa();
  Canoa *c5 = new Canoa();
  Canoa *c6 = new Canoa();

  Submarino *s1 = new Submarino();
  Submarino *s2 = new Submarino();
  Submarino *s3 = new Submarino();
  Submarino *s4 = new Submarino();

  Porta_avioes *p1 = new Porta_avioes();
  Porta_avioes *p2 = new Porta_avioes();

  c1->enderecar(12, 0);
  c2->enderecar(10, 8);
  c3->enderecar(10, 10);
  c4->enderecar(1, 11);
  c5->enderecar(12, 7);
  c6->enderecar(6, 5);

  s1->enderecar(2, 5, 'd');
  s2->enderecar(4, 1, 'd');
  s3->enderecar(12, 7, 'e');
  s4->enderecar(6, 5, 'd');

  p1->enderecar(5, 7, 'c');
  p2->enderecar(9, 5, 'e');

  player1.push_back(c1);
  player1.push_back(c2);
  player1.push_back(c3);
  player1.push_back(c4);
  player1.push_back(c5);
  player1.push_back(c6);

  player1.push_back(s1);
  player1.push_back(s2);
  player1.push_back(s3);
  player1.push_back(s4);

  player1.push_back(p1);
  player1.push_back(p2);

  Canoa *c_1 = new Canoa();
  Canoa *c_2 = new Canoa();
  Canoa *c_3 = new Canoa();
  Canoa *c_4 = new Canoa();
  Canoa *c_5 = new Canoa();
  Canoa *c_6 = new Canoa();

  Submarino *s_1 = new Submarino();
  Submarino *s_2 = new Submarino();
  Submarino *s_3 = new Submarino();
  Submarino *s_4 = new Submarino();

  Porta_avioes *p_1 = new Porta_avioes();
  Porta_avioes *p_2 = new Porta_avioes();

  c_1->enderecar(0, 7);
  c_2->enderecar(1, 12);
  c_3->enderecar(6, 10);
  c_4->enderecar(7, 12);
  c_5->enderecar(11, 2);
  c_6->enderecar(12, 11);

  s_1->enderecar(6, 1, 'd');
  s_2->enderecar(7, 3, 'd');
  s_3->enderecar(3, 10, 'e');
  s_4->enderecar(9, 8, 'd');

  p_1->enderecar(4, 0, 'c');
  p_2->enderecar(6, 6, 'e');

  player2.push_back(c_1);
  player2.push_back(c_2);
  player2.push_back(c_3);
  player2.push_back(c_4);
  player2.push_back(c_5);
  player2.push_back(c_6);

  player2.push_back(s_1);
  player2.push_back(s_2);
  player2.push_back(s_3);
  player2.push_back(s_4);

  player2.push_back(p_1);
  player2.push_back(p_2);

  int coluna;
  int linha;

  cout << endl << endl;
  cout << "jogador 1 ";
  cout << j1->get_nome();
  cout << " comecara jogadando!!!" << endl;
  cout << "o jogador 2 ";
  cout << j2->get_nome();
  cout <<  " jogara apos a jogada do jogador 1" << endl << endl << endl;

  int pontuacao1;
  int pontuacao2;


  while(j1->get_pontuacao() != 10 || j2->get_pontuacao() != 10){

    j1->imprimi_mapa_inimigo();
    cout << endl << endl;
    cout << "jogador 1 escolha no mapa inimigo onde que atacar" << endl;
    cout << "digite a linha em que quer jogar: ";
    linha = getInput<int>();
    cout << endl;
    cout << "digite a coluna em que quer jogar: ";
    coluna = getInput<int>();
    cout << endl;

    for(Embarcacao * e : player1){
      e->perder_vida(linha, coluna);
      if(e->get_tiro() == "ERROU"){
          j1->lancar_missel(linha, coluna, "nada");
      }
      if(e->get_tiro() == "ACERTOU"){
        j1->lancar_missel(linha, coluna, e->get_nome());
        pontuacao1 ++;
        j1->set_pontuacao(pontuacao2);
        break;
      }
    }

    system("clear");

    j2->imprimi_mapa_inimigo();
    cout << endl << endl;
    cout << "jogador 2 escolha no mapa inimigo onde que atacar" << endl;
    cout << "digite a linha em que quer jogar: ";
    linha = getInput<int>();
    cout << endl;
    cout << "digite a coluna em que quer jogar: ";
    coluna = getInput<int>();
    cout << endl;

    for(Embarcacao * e : player2){
      e->perder_vida(linha, coluna);
      if(e->get_tiro() == "ERROU"){
          j2->lancar_missel(linha, coluna, "nada");
      }
      if(e->get_tiro() == "ACERTOU"){
        j2->lancar_missel(linha, coluna, e->get_nome());
        pontuacao1 ++;
        j1->set_pontuacao(pontuacao2);
        break;
      }

    }
  }

























  return 0;
}
