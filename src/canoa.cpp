#include "canoa.hpp"
#include <string>
#include <iostream>

using namespace std;

Canoa::Canoa(){
  set_nome("Canoa");
  endereco = new int[2];
}

Canoa::~Canoa(){
  cout << "CANOA FOI DESTRUIDA!!!" << endl;
}

void Canoa::perder_vida(int linha, int coluna){
  set_tiro("ERROU");
  if(linha == endereco[1] && coluna == endereco[2]){
    set_vida(0);
    cout << endl << "acertou a canoa!!!" << endl;
    set_tiro("ACERTOU");
  }

}

void Canoa::enderecar(int linha, int coluna){
  endereco[1] = linha;
  endereco[2] = coluna;
}
