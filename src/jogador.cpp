#include "jogador.hpp"
#include <iostream>
#include <string>

using namespace std;

Jogador::Jogador(){
  nome = "";
  mapa = new char[13*13];
  for(int i = 0; i < 13*13; i++){
    mapa[i] = '0';
  }
  qtd_de_embarcacoes = 12;
  pontuacao = 0;
}

Jogador::~Jogador(){
  cout << "VOCE FOI DESTRUIDO";
}


void Jogador::imprimi_mapa_inimigo(){
  //cout << "-----------------------------------" << endl;
  cout << "   1  2  3  4  5  6  7  8  9  10 11 12 13" << endl;
  for(int i = 0; i < 13; i++){
    if(i < 9){
      cout << i+1 << "  ";
    }
    else{
      cout << i+1 << " ";
    }
    for(int j = 0; j < 13; j++){
      cout << mapa[i] << "  ";
    }
    cout << endl;
  }
}

void Jogador::lancar_missel(int linha, int coluna, string embarcacao){

  int indice;
  indice = ((linha - 1) * 13) + (coluna - 1);

  if(embarcacao == "nada"){
    mapa[indice] = 'X';
  }
  else if(embarcacao == "Submarino"){
    mapa[indice] = 'S';
  }
  else if(embarcacao == "Canoa"){
    mapa[indice] = 'C';
  }
  else if (embarcacao == "Porta avioes"){
    mapa[indice] = 'P';
  }
}

string Jogador::get_nome(){
  return nome;
}
void Jogador::set_nome(string nome){
  this->nome = nome;
}

int Jogador::get_qtd_de_embarcacoes(){
  return qtd_de_embarcacoes;
}
void Jogador::set_qtd_de_embarcacoes(int qtd_de_embarcacoes){
  this->qtd_de_embarcacoes = qtd_de_embarcacoes;
}

int Jogador::get_pontuacao(){
  return pontuacao;
}
void Jogador::set_pontuacao(int pontuacao){
  this->pontuacao = pontuacao;
}
