#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP
#include <string>
#include "embarcacao.hpp"

using namespace std;

class Submarino : public Embarcacao{
private:
  int *endereco;
  int *qtd_de_tiros;

public:
  Submarino();
  ~Submarino();

  void perder_vida(int linha, int coluna);
  void enderecar(int linha, int coluna, char direcao);

};

#endif
