#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP
#include <iostream>
#include "embarcacao.hpp"

class Porta_avioes : public Embarcacao{
private:
  int *endereco;
  int *qtd_de_tiros;

public:
  Porta_avioes();
  ~Porta_avioes();

  void perder_vida(int linha, int coluna);
  void enderecar(int linha, int coluna, char direcao);
};
#endif
