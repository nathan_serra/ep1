#ifndef CANOA_HPP
#define CANOA_HPP
#include <string>
#include "embarcacao.hpp"

using namespace std;

class Canoa : public Embarcacao{
private:
  int *endereco;

public:

  Canoa();
  ~Canoa();

  void perder_vida(int linha, int coluna);
  void enderecar(int linha, int coluna);

};

#endif
