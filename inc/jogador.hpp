#ifndef JOGADOR_hpp
#define JOGADOR_hpp

#include <string>

using namespace std;

class Jogador{
private:
  string nome;
  char *mapa;
  int qtd_de_embarcacoes;
  int pontuacao;

public:
  Jogador();
  ~Jogador();

  void lancar_missel(int linha, int coluna, string embarcacao);
  void imprimi_mapa_inimigo();

  string get_nome();
  void set_nome(string nome);

  int get_qtd_de_embarcacoes();
  void set_qtd_de_embarcacoes(int qtd_de_embarcacoes);

  int get_pontuacao();
  void set_pontuacao(int pontuacao);
};

#endif
