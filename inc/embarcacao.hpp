#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP
#include <string>

using namespace std;

class Embarcacao{
private:
  string nome;
  int vida;
  string tiro;

public:

  Embarcacao();
  ~Embarcacao();

   void perder_vida() = 0;

  string get_nome();
  void set_nome(string nome);

  string get_tiro();
  void set_tiro(string tiro);

  int get_vida();
  int set_vida(int vida);

};

#endif
